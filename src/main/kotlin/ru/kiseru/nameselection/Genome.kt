package ru.kiseru.nameselection

import kotlin.random.Random

data class Genome(
    private val name: String,
) {

    fun getBadCharactersCount(target: String): Int {
        assertLengthEquality(target)
        return (name.indices).count { name[it] != target[it] }
    }

    fun mate(genome: Genome): Genome {
        assertLengthEquality(genome.name)
        val newName = createNewNameFrom(genome)
        return Genome(newName)
    }

    fun mutate(): Genome {
        val name = createNewName()
        return Genome(name)
    }

    private fun createNewName(): String {
        val position = fetchRandomPosition()
        val newChar = fetchRandomLatinLetter()
        return name.substring(0, position) + newChar + name.substring(position + 1)
    }

    private fun fetchRandomLatinLetter(): Char =
        Random.nextInt('A'.code, 'Z'.code + 1).toChar()

    private fun createNewNameFrom(genome: Genome): String {
        val position = fetchRandomPosition()
        return name.substring(0, position) + genome.name.substring(position)
    }

    private fun fetchRandomPosition(): Int =
        Random.nextInt(0, name.length)

    private fun assertLengthEquality(target: String) =
        require(name.length == target.length) { "target must have the same length as name" }
}